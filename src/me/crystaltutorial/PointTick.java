package me.crystaltutorial;

import java.util.List;

import me.crystaltutorial.data.PlayerData;
import me.crystaltutorial.data.Point;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

public class PointTick extends BukkitRunnable {
	
	private static Main plugin;
	
	public PointTick(Main i){
		plugin = i;
	}

	@Override
	public void run() {
		for(Point p : Point.getAllPoints()){
			List<Entity> e = p.getDroppedItem().getNearbyEntities(1, 1, 1);
			for(Entity ent : e){
				if(ent instanceof Player){
					Player play = (Player) ent;
					PlayerData pd = plugin.getPlayerDataHandler().getPlayerData(play.getUniqueId());
					if(pd.getCompletedIDs().contains(p.getID())) continue;
					play.sendMessage(ChatColor.translateAlternateColorCodes('&', p.getMessage()));
					pd.getCompletedIDs().add(p.getID());
					if(pd.getCompletedIDs().size() == Point.getAllPoints().size()){
						finishedTutorial(play);
						pd.makeFinished();
					}
				}
			}
		}
	}
	
	public static void finishedTutorial(final Player p){
		Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable(){
			public void run(){
				p.sendMessage(ChatColor.GREEN + "You have completed the Tutorial!");
				p.teleport(new Location(Bukkit.getWorld("world"), -729.5, 41.5, 2903.5, 0, 0));
				p.sendMessage(ChatColor.YELLOW + "You have been returned to Spawn!");
			}
		}, 40L);
	}

}
