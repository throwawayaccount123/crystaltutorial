package me.crystaltutorial;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public final class Util 
{		
	private Util() {}
	
	public static Location getLocation(World w, String path)
	{
		double x = Main.plugin.getConfig().getDouble("Points." + path + ".X");
		double y = Main.plugin.getConfig().getDouble("Points." + path + ".Y");
		double z = Main.plugin.getConfig().getDouble("Points." + path + ".Z");
		
		return new Location(w, x, y, z);
	}
	
	public static String getMessage(String path)
	{
		String message = Main.plugin.getConfig().getString("Points." + path + ".Message");
		message = ChatColor.translateAlternateColorCodes('&', message);
		
		return message;
	}
	
	public static ItemStack getNetherStar()
	{
		ItemStack i = new ItemStack(Material.NETHER_STAR);
		ItemMeta im = i.getItemMeta();
		
		im.setDisplayName(ChatColor.GREEN + "Crystal");
		i.setItemMeta(im);
		return i;
	}
}
