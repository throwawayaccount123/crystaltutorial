package me.crystaltutorial.commands;

import me.crystaltutorial.Main;
import me.crystaltutorial.data.Point;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class PointCommand implements CommandExecutor
{
	
	private Main plugin;
	
	public PointCommand(Main i){
		plugin = i;
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) // /point add Shop : THIS IS THE MESSAGE
	{
		if(cmd.getName().equalsIgnoreCase("point"))
		{
			if(sender instanceof Player && sender.isOp())
			{
				Player p = (Player) sender;

				if(args.length > 1)
				{
					if(args[0].equalsIgnoreCase("add"))
					{
						String addedArgs = "";

						for(int i = 1; i < args.length; i++)
						{
							addedArgs +=  " " + args[i];
						}

						String[] parts = addedArgs.split(":");  //First index of parts array is the name of the point. Second is the message. parts[0] = Shop , parts[1] =  THIS IS THE MESSAGE

						if(parts[1].charAt(0) == ' ')
						{
							parts[1] = parts[1].substring(1);
						}
						
						parts[0] = parts[0].replaceAll(" ", "");
						
						new Point(p, parts[1], parts[0]);
						p.sendMessage(ChatColor.YELLOW + "Added new point " + ChatColor.BLUE + parts[0] + ChatColor.YELLOW +
								" at coordinates: " + ChatColor.BLUE + p.getLocation().getBlockX() + "," + p.getLocation().getBlockY() + "," + p.getLocation().getBlockZ() +
								ChatColor.YELLOW + " with the message: " + parts[1]);
					}
					
					else if(args[0].equalsIgnoreCase("remove"))
					{
						Point.removePoint(p, args[1]);
					}
					else if(args[0].equalsIgnoreCase("reload")){
						plugin.getDataHandler().savePoints();
						plugin.getDataHandler().loadPoints();
						sender.sendMessage("If there were no errors in the console, reloading has been successful :)");
					}
				}
			}
		}
		return false;
	}
}
