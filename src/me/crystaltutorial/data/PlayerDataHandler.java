package me.crystaltutorial.data;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.UUID;

import me.crystaltutorial.Main;

public class PlayerDataHandler {
	
	private Main plugin;
	
	public PlayerDataHandler(Main i){
		plugin = i;
	}
	
	public void saveData(PlayerData pd) {
        File f = new File(plugin.getDataFolder(), pd.getID().toString());
        try {
            if (!(f.exists())) {
                f.createNewFile();
            }
            ObjectOutputStream oss = new ObjectOutputStream(new FileOutputStream(f));
            oss.writeObject(pd);
            oss.flush();
            oss.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
		plugin.getLogger().info("Saved PD for " + pd.getID());
		if(PlayerData.getDataList().containsKey(pd.getID())){
			PlayerData.getDataList().remove(pd.getID());
		}
	}

	public PlayerData loadPlayerData(UUID id){
        File f = new File(plugin.getDataFolder(), id.toString());
        if (!f.exists()) {
        	saveData(new PlayerData(id));
        }
        try {
            ObjectInputStream iss = new ObjectInputStream(new FileInputStream(f));
            Object o = iss.readObject();
            iss.close();
            PlayerData.getDataList().put(id, (PlayerData)o);
    		if(!PlayerData.getDataList().containsKey(((PlayerData) o).getID())){
    			PlayerData.getDataList().put(id, (PlayerData)o);
    		}
            return (PlayerData) o;
        } catch(Exception e) {
    		e.printStackTrace();
    	}
        return null;
	}
	
	public PlayerData getPlayerData(UUID id){
		if(PlayerData.getDataList().containsKey(id)){
			return PlayerData.getDataList().get(id);
		}
		return loadPlayerData(id);
	}
}
