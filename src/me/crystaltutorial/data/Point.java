package me.crystaltutorial.data;

import java.io.Serializable;
import java.util.ArrayList;

import me.crystaltutorial.Util;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;

public class Point implements Serializable {

	private static final long serialVersionUID = 1L;
	private Location loc;
	private String message;
	private String name;
	private int ID;
	private Item droppedItem;
	private static ArrayList<Point> allPoints = new ArrayList<Point>();
	
	public Point(Player p, String mes, String nam){
		if(getPoint(nam) != null){
			p.sendMessage(ChatColor.RED + "There is already a point with this name! Please pick another one <3");
		}
		loc = p.getLocation();
		message = mes;
		name = nam;
		this.ID = allPoints.size() + 1;
		droppedItem = p.getWorld().dropItem(p.getLocation(), Util.getNetherStar());
		allPoints.add(this);
	}
	
	public Location getLocation(){
		return this.loc;
	}
	
	public String getMessage(){
		return this.message;
	}
	
	public String getName(){
		return this.name;
	}
	
	public int getID(){
		return this.ID;
	}
	
	public Item getDroppedItem(){
		return this.droppedItem;
	}
	
	public static ArrayList<Point> getAllPoints(){
		return allPoints;
	}
	
	public static Point getPoint(String pointName){
		Point p = null;
		for(Point point : allPoints){
			if(point.getName().equals(pointName)){
				p = point;
				break;
			}
		}
		return p;
	}
	
	public static boolean removePoint(Player pl, String pointName){
		Point p = getPoint(pointName);
		if(p != null){
			allPoints.remove(p);
			pl.sendMessage(ChatColor.GREEN + "Point has been removed successfully!");
			return true;
		}
		pl.sendMessage(ChatColor.RED + "Point removal was not successful!");
		return false;
	}
	
}
