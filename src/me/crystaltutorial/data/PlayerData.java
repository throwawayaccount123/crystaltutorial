package me.crystaltutorial.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

public class PlayerData implements Serializable {

	private static final long serialVersionUID = 1L;
	private ArrayList<Integer> completedIDs;
	private boolean finished;
	private UUID id;
	private boolean inOpt;
	private static HashMap<UUID, PlayerData> dataList = new HashMap<UUID, PlayerData>();
	
	public PlayerData(UUID id){
		this.completedIDs = new ArrayList<Integer>();
		this.finished = false;
		this.id = id;
		this.inOpt = true;
	}
	
	public ArrayList<Integer> getCompletedIDs(){
		return this.completedIDs;
	}
	
	public boolean getFinished(){
		return this.finished;
	}
	
	public void makeFinished(){
		this.finished = true;
	}
	
	public UUID getID(){
		return this.id;
	}
	
	public boolean inOpt(){
		return this.inOpt;
	}
	
	public static HashMap<UUID, PlayerData> getDataList(){
		return dataList;
	}
}
