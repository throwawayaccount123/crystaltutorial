package me.crystaltutorial.data;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import me.crystaltutorial.Main;

public class DataHandler {

	private Main plugin;

	public DataHandler(Main i){
		plugin = i;
	}
	public void savePoints() {
		File f = new File(plugin.getDataFolder(), "Points.yml");
		ArrayList<Point> points = Point.getAllPoints();
		try {
			if (!(f.exists())) {
				f.createNewFile();
			}
			ObjectOutputStream oss = new ObjectOutputStream(new FileOutputStream(f));
			oss.writeObject(points);
			oss.flush();
			oss.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		plugin.getLogger().info("Saved all the points in the data =)");
	}

	@SuppressWarnings("unchecked")
	public void loadPoints(){
		File f = new File(plugin.getDataFolder(), "Points.yml");
		if (!f.exists()) {
			savePoints();
		}
		try {
			ObjectInputStream iss = new ObjectInputStream(new FileInputStream(f));
			Object o = iss.readObject();
			iss.close();
			Point.getAllPoints().clear();
			ArrayList<Point> points = (ArrayList<Point>) o;
			for(Point p : points){
				Point.getAllPoints().add(p);
				//We may have to re-drop entities here.
			}
			plugin.getLogger().info("Loaded points correctly.. HUZZAH!");
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
}
