package me.crystaltutorial.servermenu;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import me.crystaltutorial.Main;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Blaze;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;

public class ServerMenu implements Listener{

	private List<ItemStack> heist = new ArrayList<>();
	private List<ItemStack> survival = new ArrayList<>();
	
	public HashMap<Player, EntityType> spawnedPets = new HashMap<Player, EntityType>();

	int ID1;
	int ID2;

	ByteArrayDataOutput out = ByteStreams.newDataOutput();

	private int num = 0;

	ItemStack compass = new ItemStack(Material.COMPASS, 1);
	ItemMeta cmeta = compass.getItemMeta();

	ItemStack pets = new ItemStack(Material.BONE);
	ItemMeta pmeta = pets.getItemMeta();


	@EventHandler
	public void onJoin(PlayerJoinEvent event){
		Player p = event.getPlayer();
		giveJoinItems(p);
	}

	@EventHandler
	public void onClick(PlayerInteractEvent event){
		Player p = event.getPlayer();
		Action a = event.getAction();
		if(a == Action.RIGHT_CLICK_AIR || a == Action.RIGHT_CLICK_BLOCK){
			if(event.getItem().getType() == Material.COMPASS){
				createServerMenu(p);
			}
		}
	}


	@EventHandler
	public void onInvClick(InventoryClickEvent event){
		Player p = (Player)event.getWhoClicked();
		ItemStack clicked = event.getCurrentItem();
		Inventory inv = event.getInventory();
		if(inv.getName() == "§a§lServer Selector"){
			if(clicked.getType() == Material.IRON_FENCE){
				out.writeUTF("Connect");
				out.writeUTF("Heist");
				p.sendPluginMessage(Main.getInstance(), "BungeeCord", out.toByteArray());
				p.closeInventory();
				p.sendMessage("§c§l>§e§l> §aConnecting you to Heist!");
			}
			else if(clicked.getType() == Material.DIAMOND_SWORD){
				out.writeUTF("Connect");
				out.writeUTF("Survival");
				p.sendPluginMessage(Main.getInstance(), "BungeeCord", out.toByteArray());
				p.closeInventory();
				p.sendMessage("§c§l>§e§l> §aConnecting you to Survival!");
			}
		}
		if(inv.getName() == "§e§lPets"){
			
			/*Blaze*/
			if(clicked.getType() == Material.BLAZE_ROD){
				Blaze b = (Blaze) p.getWorld().spawnEntity(p.getLocation(), EntityType.BLAZE);
				b.setCustomName("§a"+p.getName() + "'s Pet");
				b.setCustomNameVisible(true);
				spawnedPets.put(p, EntityType.BLAZE);
			}
			
			/*Wolf*/
			if(clicked.getType() == Material.BONE){
				Blaze b = (Blaze) p.getWorld().spawnEntity(p.getLocation(), EntityType.WOLF);
				b.setCustomName("§a"+p.getName() + "'s Pet");
				b.setCustomNameVisible(true);
				spawnedPets.put(p, EntityType.WOLF);
			}
		}
		event.setCancelled(true);
	}


	@EventHandler
	public void onInvClose(InventoryCloseEvent event){
		Bukkit.getScheduler().cancelTask(ID1);
	}


	@EventHandler
	public void onDrop(PlayerDropItemEvent event){
		Player p = event.getPlayer();
		event.setCancelled(true);
		p.sendMessage("§cYou are not allowed to drop that.");
	}

	public void giveJoinItems(Player p){
		ArrayList<String> clore = new ArrayList<String>();
		clore.add("§7§oUse me to select a server!");
		cmeta.setLore(clore);
		cmeta.setDisplayName("§a§lServer Selector");
		compass.setItemMeta(cmeta);

		ArrayList<String> plore = new ArrayList<String>();
		plore.add("§7§oUse me to select a pet!");
		pmeta.setDisplayName("§e§lPet Selector");
		pmeta.setLore(plore);
		pets.setItemMeta(pmeta);

		p.getInventory().setItem(0, compass);
		p.getInventory().setItem(8, pets);
	}

	public void createServerMenu(Player p){
		Inventory inv = Bukkit.createInventory(null, 9, "§a§lServer Selector");

		/*Heist*/

		heist.add(make(Material.IRON_FENCE, 1, 0, "§b§oHeist", "  §aClick to connect"));
		heist.add(make(Material.IRON_FENCE, 1, 0, "§b§oHeist", "§2► §aClick to connect"));
		survival.add(make(Material.DIAMOND_SWORD, 1, 0, "§c§oSurvival", "  §aClick to connect"));
		survival.add(make(Material.DIAMOND_SWORD, 1, 0, "§c§oSurvival", "§2► §aClick to connect"));
		ID1 = Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.getInstance(), new Runnable() {
			public void run() {
				if(num == 1) {
					num = 0;
				} else {
					num++;				
				}

				inv.setItem(0, heist.get(num));
				inv.setItem(1, survival.get(num));
			}
		}, 0, 2 * 2);
		p.openInventory(inv);
	}
	private ItemStack make(Material material, int amount, int shrt, String displayname, String lore) {
		ItemStack item = new ItemStack(material, amount, (short) shrt);
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName(displayname);
		meta.setLore(Arrays.asList(lore));
		item.setItemMeta(meta);
		return item;
	}



	/*Pets*/
	public void namePet(){

	}

	public void despawnPet(){

	}

	public void setPetHat(){

	}
}
