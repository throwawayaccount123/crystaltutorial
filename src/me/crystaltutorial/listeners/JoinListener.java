package me.crystaltutorial.listeners;

import java.util.ArrayList;
import java.util.UUID;

import me.crystaltutorial.Main;
import me.crystaltutorial.PointTick;
import me.crystaltutorial.data.PlayerData;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;

@SuppressWarnings("deprecation")
public class JoinListener implements Listener {
	
	private Main plugin;
	private Location spawn = new Location(Bukkit.getWorld("world"), -729.5, 41.5, 2903.5, 0, 0);
	private Location tutorialSpawn = new Location(Bukkit.getWorld("world"), -729.5, 55, 2903.5, 0, 0); //WE WILL NEED TO FILL IN THE ACTUAL VALUES
	private ArrayList<UUID> inOptChat = new ArrayList<UUID>();
	
	public JoinListener(Main i){
		plugin = i;
	}

	@EventHandler
	public void pJoinEvent(PlayerJoinEvent e){
		PlayerData pd = plugin.getPlayerDataHandler().getPlayerData(e.getPlayer().getUniqueId()); //Loads PlayerData
		if(pd.inOpt() == true){
			offerOptOut(e.getPlayer());
			return;
		}
		if(pd.getFinished() == true){
			e.getPlayer().teleport(spawn);
		}else{
			e.getPlayer().teleport(tutorialSpawn);
			e.getPlayer().sendMessage(ChatColor.YELLOW + "If you ever wish to quit the tutorial, enter the command " + ChatColor.BLUE + "/quit");
		}
	}
	
	private void offerOptOut(Player p){
		inOptChat.add(p.getUniqueId());
		p.sendMessage(ChatColor.GREEN + "You can choose to take a tutorial explaining the " + ChatColor.BLUE + "Behemoth Network Hub.");
		p.sendMessage(ChatColor.YELLOW + "If you wish to take the tutorial, enter the message: " + ChatColor.AQUA + "yes");
		p.sendMessage(ChatColor.YELLOW + " If you would prefer to opt out of the tutorial, enterthe message: " + ChatColor.AQUA + "no");
	}
	
	@EventHandler
	public void pQuitEvent(PlayerQuitEvent e){
		plugin.getPlayerDataHandler().saveData(plugin.getPlayerDataHandler().getPlayerData(e.getPlayer().getUniqueId()));; //savesData
		if(inOptChat.contains(e.getPlayer().getUniqueId())){
			inOptChat.remove(e.getPlayer().getUniqueId());
		}
	}
	
	@EventHandler
	public void pChat(PlayerChatEvent e){
		if(inOptChat.contains(e.getPlayer().getUniqueId())){
			if(e.getMessage().equalsIgnoreCase("yes")){
				inOptChat.remove(e.getPlayer().getUniqueId());
				e.getPlayer().teleport(tutorialSpawn);
				e.getPlayer().sendMessage(ChatColor.YELLOW + "If you ever wish to quit the tutorial, enter the command " + ChatColor.BLUE + "/quit");
			}else if(e.getMessage().equalsIgnoreCase("no")){
				inOptChat.remove(e.getPlayer().getUniqueId());
				PointTick.finishedTutorial(e.getPlayer());
			}else{
				e.getPlayer().sendMessage(ChatColor.GREEN + "You can choose to take a tutorial explaining the " + ChatColor.BLUE + "Behemoth Network Hub.");
				e.getPlayer().sendMessage(ChatColor.YELLOW + "If you wish to take the tutorial, enter the message: " + ChatColor.AQUA + "yes");
				e.getPlayer().sendMessage(ChatColor.YELLOW + " If you would prefer to opt out of the tutorial, enterthe message: " + ChatColor.AQUA + "no");
			}
			e.setCancelled(true);
		}
	}
	
	@EventHandler
	public void pMoveEvent(PlayerMoveEvent e){
		Player p = e.getPlayer();
		if(!(e.getTo().getBlockX() != e.getFrom().getBlockX() || e.getTo().getBlockZ() != e.getFrom().getBlockZ())) return;
		if(inOptChat.contains(e.getPlayer().getUniqueId())){
			Location from=e.getFrom();
			Location to=e.getTo();
			double x=Math.floor(from.getX());
			double z=Math.floor(from.getZ());
			if(Math.floor(to.getX())!=x||Math.floor(to.getZ())!=z){
				x+=.5;
				z+=.5;
				p.teleport(new Location(from.getWorld(),x,from.getY(),z,from.getYaw(),from.getPitch()));
			}
			e.getPlayer().sendMessage(ChatColor.RED + "Please opt in or out of the tutorial before exploring the Hub!");
		}
	}
}
