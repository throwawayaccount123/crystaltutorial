package me.crystaltutorial.listeners;

import me.crystaltutorial.Util;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.ItemDespawnEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;

public class Pickup implements Listener
{
	@EventHandler
	public void onNetherStarPickup(PlayerPickupItemEvent e)
	{
		if(e.getItem().getItemStack().equals(Util.getNetherStar()))
		{
			e.setCancelled(true);
		}
	}
	
	@EventHandler
	public void starDespawnEvent(ItemDespawnEvent e) {
		if (e.getEntity().getItemStack().equals(Util.getNetherStar())) {
			e.setCancelled(true);
		}
	}
	
	@EventHandler
	public void blockDestroy(BlockBreakEvent e){
		if(e.getPlayer() != null){
			if(!(e.getPlayer().hasPermission("tutorial.Block"))){
				e.setCancelled(true);
			}
		}
	}
	
	@EventHandler
	public void blockPlace(BlockPlaceEvent e){
		if(e.getPlayer() != null){
			if(!(e.getPlayer().hasPermission("tutorial.Block"))){
				e.setCancelled(true);
			}
		}
	}
}
