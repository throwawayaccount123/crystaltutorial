# README #

### What is this repository for? ###

This is the Repository for the CrystalTutorial, in which players go around a fake spawn and collect stars, and when they have all the stars, they can leave the tutorial.

### How do I get set up? ###

We'll teach you how to connect to this repository via Git. If you are not currently using Eclipse, please switch to that IDE unless you have previous experience with Sourcetree or commandline.

### Contribution guidelines ###

Whenever you're adding new code, please mention in the Behemoth Dev Skype chat what classes you plan on editing.
Do not overwrite someone else's code unless you know specifically what you are doing will help the core integrity of the plugin.
Chances are, core data storage methods do not need to be edited; henceforth, DO NOT EDIT THEM.

**Things to keep in mind while coding:**
1. Try to limit use of PlayerMoveEvent. If inevitable, ensure that you are doing a few checks to lower on usage. For example, instead of using PlayerMoveEvent to check if a player is near a (set of) location(s), spawn a mob and use getNearbyEntities to check every 10 ticks or so. 
2. Do not constantly pull values from the Configuration as this uses A LOT of I/O power. If a method is consistently called in which you must pull a value from the config, instead save the value as a *static class object* (at the top of the class, outside any method)
3. Never use String names or Player Objects in ArrayLists/HashMaps, only use Players UUID's, as this allows for faster lookup and prevents any issue with players changing names.
4. Try to commentate your code to let people know what certain methods are/do. Commentating object classes is not needed.

### Who do I talk to? ###

If you have any questions or need help getting set up with Git, please just message in the Behemoth Dev Skype chat. Someone will get to you :)